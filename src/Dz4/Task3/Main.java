package Dz4.Task3;

import java.util.ArrayList;
import java.util.Scanner;
/*
3. На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Необходимо вывести матрицу на экран, каждый элемент
которого состоит из суммы индекса столбца и строки этого же элемента. Решить
необходимо используя ArrayList.
*/

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        ArrayList<Integer> matrix = new ArrayList<>(n * m);
        int index = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matrix.add(i + j);
                System.out.print(matrix.get(index) + " ");
                index++;
            }
            System.out.println();
        }
    }
}
