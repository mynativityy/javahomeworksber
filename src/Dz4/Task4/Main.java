package Dz4.Task4;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Participant participant = new Participant();
       Dog dog = new Dog();
       Rating rating = new Rating();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
            participant.addParticipant(n);
            dog.addDog(n);
            rating.addRating1(n);
        System.out.println();

/*        System.out.println(participant.participants);
        System.out.println(dog.dogs);
        System.out.println(rating.arr.get(0));
        System.out.println(rating.ratings);
        System.out.println(rating.firstPlace());
        System.out.println(rating.secondPlace());
        System.out.println(rating.thirdPlace());
        System.out.println();
       System.out.println();      */
        System.out.println(participant.participants.get(rating.firstPlace())+": "+dog.dogs.get(rating.firstPlace())+", "+rating.ratings.get(rating.firstPlace()));
        System.out.println(participant.participants.get(rating.secondPlace())+": "+dog.dogs.get(rating.secondPlace())+", "+rating.ratings.get(rating.secondPlace()));
        System.out.println(participant.participants.get(rating.thirdPlace())+": "+dog.dogs.get(rating.thirdPlace())+", "+rating.ratings.get(rating.thirdPlace()));
    }
}
