package Dz4.Task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Rating {
    private int index2;
    private int index3;
    Scanner scanner = new Scanner(System.in);


     public ArrayList<Double> ratings = new ArrayList<Double>();
    List<ArrayList<Integer>> arr = new ArrayList();
    public void addRating1(int n) {
        int[][] nums = new int[n][3];
        for (int i = 0; i < n; i++) {
            double result = 0;
            ArrayList<Integer> arr1 = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                nums[i][j] = scanner.nextInt();
                result = result + nums[i][j];
                arr1.add(nums[i][j]);
            }
            arr.add(arr1);
            ratings.add( (int)((result/3)* 10) / 10.0 );
        }
    }

    public int firstPlace(){
        double max1 = 0;
        double max2 = 0;
        double max3 = 0;
        int index1 = 0;
        int index2 = 0;
        int index3 = 0;
        for(int i=0;i<ratings.size();i++) {
            if (ratings.get(i) > max1) {
                max3 = max2;
                index3 = index2;
                max2 = max1;
                index2 = index1;
                max1 = ratings.get(i);
                index1 = i;

            } else if (ratings.get(i) > max2) {
                max3 = max2;
                index3 = index2;
                max2 = ratings.get(i);
                index2 = i;

            } else if (ratings.get(i) > max3) {
                max3 = ratings.get(i);
                index3 = i;
            }
        }
        thirdPlace(index3);
        secondPlace(index2);
        return index1 ;
    }
    public void thirdPlace(int index3){
       this.index3=index3;
    }
    public void secondPlace(int index2){
        this.index2=index2;
    }
    public int thirdPlace(){
        return index3;
    }
    public int secondPlace(){
        return index2;
    }


}