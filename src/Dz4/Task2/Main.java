package Dz4.Task2;
/*
2. Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К
сожалению, из Мебели он умеет чинить только Табуретки, а Столы, например,
нет. Реализовать метод в цеху, позволяющий по переданной мебели
определять, сможет ли ей починить или нет. Возвращать результат типа
boolean. Протестировать метод.
 */
public class Main {
    public static void main(String[] args) {
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();
        Chair chair = new Chair();
        Table table = new Table();
        Furniture table1 = new Chair();
        Furniture furniture = new Furniture();
        System.out.println(bestCarpenterEver.Repair(chair));
        System.out.println(bestCarpenterEver.Repair(table));
        System.out.println(bestCarpenterEver.RepairInCarpen(Table.class));
        System.out.println(bestCarpenterEver.RepairInCarpen(Chair.class));
        System.out.println(bestCarpenterEver.RepairInCarpen(Furniture.class));
        System.out.println(bestCarpenterEver.Repair(table1));
        System.out.println(bestCarpenterEver.Repair(furniture));
    }
}
