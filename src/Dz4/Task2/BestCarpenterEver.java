package Dz4.Task2;

public class BestCarpenterEver{

    public boolean Repair(Furniture furniture) {

        boolean f = false;
        if (furniture instanceof Table) {
            f = false;
        } else if (furniture instanceof Chair) {
            f = true;
        }
        return f;
    }

    public static boolean RepairInCarpen(Class obj) {

        boolean f = false;
        Furniture furniture = null;
        if (obj.equals(Table.class))
            f = false;
        else if (obj.equals(Chair.class)) {
            f = true;
        }
        return f;
    }
}
