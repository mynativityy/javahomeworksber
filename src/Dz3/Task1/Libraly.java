package Dz3.Task1;
import java.util.ArrayList;
import java.util.Scanner;

public class Libraly {
    Scanner scanner = new Scanner(System.in);

    static ArrayList<Book> books1 = new ArrayList<>();
    static ArrayList<Book> books2 = new ArrayList<>();
    static ArrayList<Integer> books3 = new ArrayList<>();

    public static void initData() {
        books1.add(new Book("автор1", "книга1"));
        books1.add(new Book("автор2", "книга2"));
        books1.add(new Book("автор3", "книга3"));
        books1.add(new Book("автор4", "книга4"));
        books1.add(new Book("автор5", "книга5"));
        books1.add(new Book("автор6", "книга6"));
        books1.add(new Book("автор7", "книга7"));

    }

    public static void getBookAuthor(String author) {
        String current = null;
        for (Book book : books1) {
            if (book.getAuthor() == author) {
                current = book.getNameOfBook();
                System.out.println(current);
            }
        }
    }

    public void getNameofBook(String nameOfBook) {

        for (Book book : books1) {
            if (book.getNameOfBook() == nameOfBook) {
                System.out.println(nameOfBook + " " + book.getAuthor());
                break;
            }
        }
    }

    public static void addBook(String author, String nameOfBook) {
        for (Book book : books1) {
            if ((book.getNameOfBook() == nameOfBook)) {
                return;
            } else {
                books1.add(new Book(author, nameOfBook));
            }
        }
    }

    public static void removeBook(String nameOfBook) {
        int count = 0;
        for (Book book : books1) {
            if (book.getNameOfBook() == nameOfBook) {
                books1.remove(count);
                break;
            } else
                count++;
        }
    }

    public void takeBook(String nameOfBook, Reader reader) {
        if (reader.getPossesing() == true) {
            System.out.println("У вас уже есть книга");

        }
        reader.setPossesing(true);
        if (reader.getId() == 0) {
            reader.setId(scanner.nextInt());
        }
        for (Book book : books1) {
            if (nameOfBook == book.getNameOfBook()) {
                books2.add(new Book(book.getAuthor(), nameOfBook));
                books3.add(reader.getId());
                removeBook(nameOfBook);
                return;
            }
        }
        System.out.println("такой книги нет или она одолжена");
    }
    public void backBook(String nameOfBook, Reader reader){
            if (reader.getPossesing()==false){
                System.out.println("У ВАС НЕТ КНИГИ");
                System.exit(1);
            }
        int count1=0;
        for (Book book : books2) {
            if (nameOfBook == book.getNameOfBook() && reader.getId()== books3.get(count1)){
                books1.add(new Book(book.getAuthor(),book.getNameOfBook()));
                books2.remove(count1);
                books3.remove(count1);
                reader.setPossesing(false);
               return;
            }
            count1++;
        }
        System.out.println("у вас нет такой книги");
    }
}