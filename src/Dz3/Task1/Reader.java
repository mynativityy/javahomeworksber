package Dz3.Task1;


public class Reader {
    private String name;
    private String surname;
    public boolean possesing;
    public int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean getPossesing() {
        return possesing;
    }

    public void setPossesing(boolean possesing) {
        this.possesing = possesing;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}




