create table flowers
(
    id serial primary key,
    namee varchar(30) unique,
    price integer not null
);

insert into flowers (namee, price)
values ('Розы', 100);
insert into flowers (namee, price)
values ('Лилии', 50);
insert into flowers (namee, price)
values ('Ромашки', 25);


select * from flowers;
commit;
