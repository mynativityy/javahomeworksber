create table buyers
(
    id serial primary key,
    namee varchar(30) not null,
    phone_number varchar(30) unique
);

insert into buyers (namee, phone_number)
values ('Иван1', '+7-999-999-99-99');
insert into buyers (namee, phone_number)
values ('Иван2', '+7-999-999-99-98');
insert into buyers (namee, phone_number)
values ('Иван3', '+7-888-888-88-88');

select * from buyers;
commit;
