create table orders
(
    id serial primary key,
    buyer_id integer references buyers(id),
    flower_id integer references flowers(id),
    quantity integer check(quantity > 0 and quantity <= 1000),
    date_created timestamp
);

insert into orders (buyer_id, flower_id, quantity, date_created)
values (1, 1, 5, now());
insert into orders (buyer_id, flower_id, quantity, date_created)
values (1, 2, 150, now() - interval '1y');
insert into orders (buyer_id, flower_id, quantity, date_created)
values (2, 3, 20, now());
insert into orders (buyer_id, flower_id, quantity, date_created)
values (2, 1, 1, now());
insert into orders (buyer_id, flower_id, quantity, date_created)
values (1, 3, 999, now());
insert into orders (buyer_id, flower_id, quantity, date_created)
values (2, 1, 500, now());

select * from orders;
commit;
