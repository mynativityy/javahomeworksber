--1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
select * from orders
join buyers on orders.buyer_id = buyers.id
and orders.id = 3;

--2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select * from orders
where buyer_id = 1
and date_created > now() - interval '1 month';

--3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select flowers.namee, orders.quantity
from flowers
join orders  on flowers.id = orders.flower_id
where orders.quantity = (select max(quantity) from orders);

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(orders.quantity * flowers.price)
from orders , flowers
where orders.flower_id = flowers.id;
