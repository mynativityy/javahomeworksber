package DZ1_PM.Task1;

import java.io.IOException;

public class MyCheckedException extends IOException {
    public MyCheckedException() {
        super();
    }

    public MyCheckedException(String message) {
        super(message);
    }
}