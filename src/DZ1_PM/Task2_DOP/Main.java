package DZ1_PM.Task2_DOP;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int[] arr = new int[m];
        for (int i = 0; i < m; i++) {
            arr[i] = scanner.nextInt();
        }
        int elementToSearch = scanner.nextInt();
        System.out.println(binarySearch(arr, elementToSearch));
    }

    static int binarySearch(int[] sortedArray, int element) {
        int left = 0;
        int right = sortedArray.length - 1;
        while (left <= right) {
            var middle = (left + right) / 2;
            int current = sortedArray[middle];

            if (current == element) {
                return middle;
            } else if (current < element) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
        return -1;
    }
}