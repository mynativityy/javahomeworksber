package DZ1_PM.Task4;

public class MyArithmeticException extends ArithmeticException{
    public MyArithmeticException(){
        super();
    }
    public MyArithmeticException(String message){
        super(message);
    }
}
