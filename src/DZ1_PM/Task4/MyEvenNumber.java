package DZ1_PM.Task4;

public class MyEvenNumber {
    public static void main(String[] args) {
        try {
            MyEvenNumber myEvenNumber = new MyEvenNumber(4);
            System.out.println("1 "+myEvenNumber.n);
            MyEvenNumber myEvenNumber2 = new MyEvenNumber(2);
            System.out.println("2 "+myEvenNumber2.n);
            MyEvenNumber myEvenNumber3 = new MyEvenNumber(1);
            System.out.println("3 "+myEvenNumber3.n);
        }
        catch (MyArithmeticException e) {
            System.out.println("Число нечетное " +e.getMessage());;
        }
    }
    private final int n;

    public MyEvenNumber(int n) {
        if (n % 2 == 0) {
            this.n = n;
        }
        else
        throw new MyArithmeticException();
    }
}


