package DZ1_PM.Task1_DOP;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int [] arr = new int[m];
        for (int i =0;i<m;i++){
            arr[i]=scanner.nextInt();
        }
        Arrays.sort(arr);
        System.out.println(arr[m-1]+" "+arr[m-2]);

    }
}
