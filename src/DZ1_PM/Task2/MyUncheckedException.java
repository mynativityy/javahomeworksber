package DZ1_PM.Task2;

public class MyUncheckedException extends RuntimeException{
    public MyUncheckedException(){
        super();
    }
    public MyUncheckedException(String message){
        super(message);
    }
}

