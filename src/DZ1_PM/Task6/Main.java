package DZ1_PM.Task6;

import java.text.ParseException;

public class Main {
    public static void main(String[] args) throws NewException, ParseException {
        FormValidator formValidator = new FormValidator();
        formValidator.checkHeight("125");
        formValidator.checkName("Sveta");
        formValidator.checkGender("Female");
        formValidator.checkBirthdate("13.07.1900");
    }
}
