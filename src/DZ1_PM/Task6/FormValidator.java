package DZ1_PM.Task6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;


public class FormValidator {

    public static void checkName(String str) throws NewException {
        if ((str.length()<=20 && str.length()>=2) && str.charAt(0)==str.toUpperCase().charAt(0)){
        }
        else
            throw new NewException("Неверный ввод имени");


    }
    public static void checkGender(String str)throws NewException {
        try {
            Gender.valueOf(str.toUpperCase());
        }
        catch (IllegalArgumentException e) {
            throw new NewException("неверный ввод пола");
        }
    }

    public void checkBirthdate(String str) throws NewException {
        if (str.matches("(([0-9][0-9])\\.[0-9][0-9])\\.([0-9][0-9][0-9][0-9])")) {

        }
        else {
            throw new NewException("Неверный формат ввода даты");
        }


try {
        LocalDate localDate = LocalDate.of(Integer.parseInt(str.substring(6, 10)),Integer.parseInt(str.substring(3, 5)),Integer.parseInt(str.substring(0, 2)));
        LocalDate localDateNow = LocalDate.now();
        LocalDate localDateStartTime = LocalDate.of(1900,01,01);
        if (ChronoUnit.DAYS.between(localDateStartTime,localDate)>=0 && ChronoUnit.DAYS.between(localDate,localDateNow)>=0){
        }
        else
            throw new NewException("Неверный ввод Даты рождения из условий");
}
      catch (DateTimeException e){
            throw new NewException("Неверный ввод Даты рождения/ такой даты не существует");}
    }

    public void checkHeight(String str) throws NewException{
        if(Double.parseDouble(str)>=0 ){
        }
        else
            throw new NewException("неверный ввод роста");
    }
}
