package DZ1_PM.Task3;

import java.io.*;
import java.util.Scanner;

import static java.lang.Character.toUpperCase;

public class WriteFilteFromTxt {
    private static final String INPUT_FILE_NAME = "input.txt";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private static final String DIRECTORY = "C:\\Users\\вика\\untitled3\\src\\DZ1_PM\\Task3";

    public static void main(String[] args) {
        try {
            ReadAndWrite();
        }
        catch (Exception e){
            System.out.println(e.getMessage()+"Ошибка");
        }

    }


    public static void ReadAndWrite() throws IOException {
        Scanner scanner = new Scanner(new File(DIRECTORY+"\\"+INPUT_FILE_NAME));
        Writer writer = new FileWriter(new File(DIRECTORY+"\\"+OUTPUT_FILE_NAME));
        String checkChar;
        StringBuilder stringBuilder = new StringBuilder();

        try (scanner; writer) {
            while (scanner.hasNext()) {
                checkChar = scanner.nextLine();

                for (int i = 0; i < checkChar.length(); i++) {
                    if (checkChar.charAt(i) >= 97 && checkChar.charAt(i) <= 122) {
                        stringBuilder.append(toUpperCase(checkChar.charAt(i)));
                    }
                    else {
                        stringBuilder.append(checkChar.charAt(i));
                    }
                }
                writer.write(stringBuilder + "\n");
                stringBuilder.delete(0, stringBuilder.length());
            }
        }
        catch (Exception o){
            System.out.println("не знаю, зачем ?");
        }
    }
}
