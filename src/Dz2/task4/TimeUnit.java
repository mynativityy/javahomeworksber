package Dz2.task4;

public class TimeUnit {
    private int hour;
    private int min;
    private int second;

    public TimeUnit(int hour) {
        if (hour >= 24 || hour < 0) {
            System.out.println("Ошибка: некорректные часы. Повторите ввод.");
        } else
            this.hour = hour;
        min = 0;
        second = 0;
    }

    public TimeUnit(int hour, int min) {
        if (hour >= 24 || hour < 0 || (min >= 60 || min < 0)) {
            System.out.println("Ошибка: некорректные часы. Повторите ввод.");
        } else {
            this.hour = hour;
            this.min = min;
        }
        second = 0;
    }

    public TimeUnit(int hour, int min, int second) {
        if (hour >= 24 || hour < 0 || (min >= 60 || min < 0) || (second >= 60 || second < 0))
            System.out.println("Ошибка: некорректные часы. Повторите ввод.");
        else {
            this.hour = hour;
            this.min = min;
            this.second = second;
        }
    }

    public void Time24() {
        if (min < 10 && second < 10)
            System.out.println(hour + ":" + "0" + min + ":" + "0" + second);
        else if (min < 10)
            System.out.println(hour + ":" + "0" + min + ":" + second);
        else if (second < 10)
            System.out.println(hour + ":" + min + ":" + "0" + second);
        else
            System.out.println(hour + ":" + min + ":" + +second);
    }

    public void Time12() {
        int hour12;
        if (hour < 12) {
            if (min < 10 && second < 10)
                System.out.println(hour + ":" + "0" + min + ":" + "0" + second + " am");
            else if (min < 10)
                System.out.println(hour + ":" + "0" + min + ":" + second + " am");
            else if (second < 10)
                System.out.println(hour + ":" + min + ":" + "0" + second + " am");
            else
                System.out.println(hour + ":" + min + ":" + +second + " am");
        }
        else if (hour == 12)
            if (min < 10 && second < 10)
                System.out.println(hour + ":" + "0" + min + ":" + "0" + second + " pm");
            else if (min < 10)
                System.out.println(hour + ":" + "0" + min + ":" + second + " pm");
            else if (second < 10)
                System.out.println(hour + ":" + min + ":" + "0" + second + " pm");
            else
                System.out.println(hour + ":" + min + ":" + +second + " pm");
        else if(hour == 0){
            if (min < 10 && second < 10)
                System.out.println(hour + ":" + "0" + min + ":" + "0" + second + " am");
            else if (min < 10)
                System.out.println(hour + ":" + "0" + min + ":" + second + " am");
            else if (second < 10)
                System.out.println(hour + ":" + min + ":" + "0" + second + " am");
            else
                System.out.println(hour + ":" + min + ":" + +second + " am");
        }
        else{
            hour -= 12;
        if (min < 10 && second < 10)
            System.out.println(hour + ":" + "0" + min + ":" + "0" + second + " pm");
        else if (min < 10)
            System.out.println(hour + ":" + "0" + min + ":" + second + " pm");
        else if (second < 10)
            System.out.println(hour + ":" + min + ":" + "0" + second + " pm");
        else
            System.out.println(hour + ":" + min + ":" + +second + " pm");
            hour += 12;
        }
    }

    public void PribavkaKPencii(int hour1, int min1, int second1) {
        second += second1;
        if (second >= 60) {
            second -= 60;
            min++;
        }
        min += min1;
        if (min >= 60) {
            min -= 60;
            hour++;
        }
        hour = hour + hour1;
        if (hour >= 24)
            hour -= 24;
    }
}

