package Dz2.task5;

public class DayOfWeek {
    private byte numberOfDay;
    private String nameOfDay;
    private final String[] namesOfDays ={"Monday","Tuesday", "Wednesday", "Thursday",
            "Friday", "Saturday", "Sunday"};
    private static final byte[] numbersOfDays = {1, 2, 3, 4, 5, 6, 7};

    public DayOfWeek(int numberOfDay) {
        this.numberOfDay = (byte)numberOfDay;
        this.nameOfDay = namesOfDays[this.numberOfDay-1];
    }

    public DayOfWeek(String nameOfDay) {
        this.nameOfDay = nameOfDay;
    }

    public byte getNumberOfDay() {
        return numberOfDay;
    }

    public String getNameOfDay() {
        return nameOfDay;
    }

    public void getDayOfWeek() {
        System.out.println(numberOfDay + " " + nameOfDay);
    }
}


