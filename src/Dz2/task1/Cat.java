package Dz2.task1;

public class Cat {
    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        int switcher = (int) (Math.random() * 3);
        if (switcher == 0) {
            eat();
        } else if (switcher == 1) {
            sleep();
        } else if (switcher == 2) {
            meow();
        }
    }
}

