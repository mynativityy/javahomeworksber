package Dz2.task3;

import Dz2.task2.Student;

public class Main {
    public static void main(String[] args) {

        Student student1 = new Student();
        student1.setName("Виктор");
        student1.setSurname("Данилов");
        Student student2 = new Student();
        Student student3 = new Student();
        student2.setName("Виктор2");
        student2.setSurname("Петров");
        student3.setName("Виктор3");
        student3.setSurname("ААФЫВ");
        student1.setGrade(new int[10]);
        student2.setGrade(new int[10]);
        student3.setGrade(new int[10]);
        System.out.println("оценки 1 студента");
        student1.addGrade();
        System.out.println("оценки 2 студента");
        student2.addGrade();
        System.out.println("оценки 3 студента");
        student3.addGrade();

        Student[] students = {student1, student2, student3};
        Student best = StudentService.bestStudent(students);
        System.out.println("Лучший студент: " + best.getSurname() + " " + best.getName() + " " + best.avgGrade());

        StudentService.sortBySurname(students);

        System.out.println("Фамилии студентов в отсортированном массиве:");
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].getSurname());
        }
    }
}
