package Dz2.task8;

public class Atm {
    private double exchangeRate;
    private static int count;
    private double rub;
    private double dollar;

    public Atm(double exchangeRate) {
        if (exchangeRate < 0) {
            System.out.println("Ошибка, такого курса не может быть");
            System.exit(1);
        } else {
            this.exchangeRate = exchangeRate;
            count++;
        }
    }

    public double rInDollar(double rub) {
        this.rub = rub;
        return  rub/exchangeRate;
    }

    public double dollarInRub(double dollar) {
        this.dollar = dollar;
        return Math.round(dollar * exchangeRate);
    }

    public static int  getCount() {
        return count;
    }

}