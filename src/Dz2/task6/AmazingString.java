package Dz2.task6;

public class AmazingString {
    private char[] chars;

    public AmazingString(char[] chars) {
        this.chars = chars;
    }

    public AmazingString(String stroka) {
        chars = new char[stroka.length()];
        for (int i = 0; i < stroka.length(); i++)
            chars[i] = stroka.charAt(i);
    }

    public char getChar(int i) {
        return chars[i];
    }

    public int getLenght() {
        return chars.length;
    }

    public void getChars() {
        for (int i = 0; i < chars.length; i++)
            System.out.print(chars[i] + " ");
    }

    public boolean check(char[] podstroka) {
        boolean b = false;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == podstroka[0]) {
                for (int j = 0; j < podstroka.length; j++) {
                    if (chars[i + j] == podstroka[j])
                        b = true;
                    else {
                        b = false;
                        break;
                    }
                }
            }
        }
        return b;
    }

    public boolean checkStroka(String podstroka1) {
        char[] podstroka123;
        podstroka123 = new char[podstroka1.length()];
        for (int i = 0; i < podstroka1.length(); i++)
            podstroka123[i] = podstroka1.charAt(i);
        return check(podstroka123);
    }

    public void deleteSpace() {
        int count = 0;
        char[] pods;
        pods = new char[chars.length];
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == ' ')
                count++;
            else
                break;
        }
        int n = 0;
        for (int i = count; i < chars.length; i++) {
            pods[n++] = chars[i];
        }
        chars = new char[pods.length - count];
        for (int i = 0; i < pods.length - count; i++) {
            chars[i] = pods[i];
            System.out.print(chars[i]);
        }
    }

    public void reverse() {
        char[] pods;
        pods = new char[chars.length];
        for (int i = 0; i < chars.length; i++) {
            pods[i] = chars[chars.length - i-1];
        }
        for (int i = 0; i < chars.length; i++) {
            chars[i] = pods[i];
            System.out.print(chars[i]);
        }
    }
}
