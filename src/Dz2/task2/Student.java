package Dz2.task2;

import java.util.Scanner;

public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];

    public void setName(String username) {

        name = username;
    }

    public String getName() {

        return name;
    }

    public void setSurname(String usersurname) {
        surname = usersurname;
    }

    public String getSurname() {

        return surname;
    }

    public void setGrade(int[] studentgrades) {
        grades = studentgrades;
    }

    public int[] getGrades() {
        return grades;
    }

    public double avg(int[] grades) {
        double result = 0;
        int count = 0;
        for (int i = 0; i < grades.length; i++) {
            result = result + grades[i];
            if (grades[i] != 0)
                count++;
        }
        result = (result / count);
        result = (int) (result * 10) / 10.0;
        System.out.println(result);
        return result;
    }

    public void addGrade() {
        Scanner scanner = new Scanner(System.in);
        int n = -1;
        int count = 0;
        while (n != 0) {
            System.out.println("Веддите оценку, или 0,если нечего вводить");
            n = scanner.nextInt();
            count++;
            if ((n == 0) || count == 10)
                break;
            for (int i = 0; i < grades.length - 1; i++) {
                grades[i] = grades[i + 1];
            }
            grades[9] = n;
        }
    }
    public double avgGrade() {
        double sum = 0;
        int count = 0;

        for (int grade : grades) {
            if (grade != 0) {
                sum += grade;
                count++;
            }
        }
        return (Math.round(sum / count * 10)) / 10.0;
    }
}





