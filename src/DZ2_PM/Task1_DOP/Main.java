package DZ2_PM.Task1_DOP;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //   int n = scanner.nextInt();
        String[] array = {"the", "day", "is", "sunny", "the", "the", "the",
                "sunny", "is", "is", "day"};

        //    for (int i = 0; i < n; i++)
        //     array[i] = scanner.next();
        int k = scanner.nextInt();
        System.out.println(mostWords(array, k));
    }


    public static  Map<String, Integer> mostWords(String[] s1, int k) {
        Map<String, Integer> result = new HashMap<>();
        for (String value : s1) {
            int counter = 1;
            if (result.containsKey(value)) {
                counter = result.get(value);
                counter++;
            }
            result.put(value, counter);
        }
        Map<String, Integer> result1 = new HashMap<>();
        System.out.println(Collections.max(result.values()));
        String s0=null;
        for (int i = 0; i < k; i++) {
            for(String value: result.keySet()){
                 if(result.containsKey(value)==true) {
                      s0 = value;
                      break;
                 }
            }
                result1.put(s0,Collections.max(result.values()));
                result.remove(s0);
              }

        return result1;
    }

}

