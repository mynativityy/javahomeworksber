package DZ2_PM.Task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Document document1 = new Document(1, "111111", 10);
        Document document2 = new Document(2, "222222222", 2);
        Document document3 = new Document(3, "3333333333", 13);
        Document document4 = new Document(5, "3333333333", 13);
        List<Document> list = new ArrayList<>();
        list.add(document1);
        list.add(document2);
        list.add(document3);
        list.add(document4);
        System.out.println(organizeDocuments(list));

    }
    public static Map<Integer, Document> organizeDocuments(List<Document> documents){
        Map<Integer, Document> result = new HashMap<>();
        for (Document e : documents) {
            result.put(e.id, e);
        }
        return result;

    }
}
