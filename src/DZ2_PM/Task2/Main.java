package DZ2_PM.Task2;


import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String t = scanner.nextLine();
        System.out.println(anagramma(s,t));

}
public static boolean anagramma(String s,String t){
        if (s.length() != t.length())
            return false;
    char[] s1 = s.toLowerCase().toCharArray();
    Arrays.sort(s1);
    char[] t1 = t.toLowerCase().toCharArray();
    Arrays.sort(t1);
    return Arrays.equals(s1, t1);
    }
}
