package DZ2_PM.Task3;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        PowerfulSet p1 = new PowerfulSet();
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);
        System.out.println(p1.intersection(set1, set2));
        Set<Integer> set3 = new HashSet<>();
        set3.add(1);
        set3.add(2);
        set3.add(3);
        Set<Integer> set4 = new HashSet<>();
        set4.add(0);
        set4.add(1);
        set4.add(2);
        set4.add(4);
        System.out.println(p1.union(set3, set4));

        Set<Integer> set5 = new HashSet<>();
        set5.add(1);
        set5.add(2);
        set5.add(3);
        Set<Integer> set6 = new HashSet<>();
        set6.add(0);
        set6.add(1);
        set6.add(2);
        set6.add(4);
        System.out.println(p1.relativeComplement(set5, set6));

    }
}
