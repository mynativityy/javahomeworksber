package DZ4_PM.Task3;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List <String> list = List.of("abc", "", "", "def", "qqq");
        System.out.println(list.stream()
                .filter(s -> s.length()>0)
                .count());
    }
}
