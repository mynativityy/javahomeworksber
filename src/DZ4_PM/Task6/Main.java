package DZ4_PM.Task6;

import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
            Set<Integer> set1 = Set.of(14, 532, 35);
            Set<Integer> set2 = Set.of(41, 52, 64);
            Set<Integer> set3 = Set.of(57, 68, 97);
            Set<Integer> set4 = Set.of(473, 438, 41);
            Set<Set<Integer>> set = Set.of(set1,set2,set3,set4);

        System.out.println(set.stream()
                    .flatMap(Set::stream)
                    .collect(Collectors.toSet()));
        }
}
