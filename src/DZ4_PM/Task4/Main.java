package DZ4_PM.Task4;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Double> list = List.of(1.1, 1.2, 41.41, 0.4,2.4);
        list.stream()
                .sorted(Collections.reverseOrder())
                .forEach(System.out::println);

    }
}
