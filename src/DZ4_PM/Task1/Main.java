package DZ4_PM.Task1;

import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        int result = IntStream.rangeClosed(1,100)
                .filter(n -> n % 2 ==0)
                .sum();
        System.out.println(result);
    }
}
