package DZ4_PM.Task1_DOP;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count=0;
        String s1 = scanner.nextLine();
        String s2 = scanner.nextLine();
        if (Math.abs(s1.length()-s2.length())>1)
            System.exit(1);
        for(int i=0;i<s1.length();i++) {
            if (s1.charAt(i) == s2.charAt(i)) {
                count++;
            }
        }
           if ((Math.abs(count-s1.length())<=1) && Math.abs(count-s2.length())<=1)
               System.out.println(true);
            else
               System.out.println(false);
    }
}
