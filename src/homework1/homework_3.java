package homework1;

import java.util.Arrays;
import java.util.Scanner;

public class homework_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int count = 0;
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
            if (array[i] < 0)
                count++;
        }
        int[] myArrayplus = new int[n - count];
        int[] myArrayminus = new int[count];
        for (int i = 0; i <n-count ; i++){
            myArrayplus[i] = (int) Math.pow(array[i+count], 2);
        }
        for (int j = 0; j < count; j++){
            myArrayminus[j] = (int) Math.pow(array[j], 2);
        }

        boolean isSorted = false;
        int buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < myArrayminus.length-1; i++) {
                if(myArrayminus[i] > myArrayminus[i+1]){
                    isSorted = false;

                    buf = myArrayminus[i];
                    myArrayminus[i] = myArrayminus[i+1];
                    myArrayminus[i+1] = buf;
                }
            }
        }
        MergeTwoArrays(myArrayminus, myArrayplus,count);
    }

    public static void MergeTwoArrays(int[] arr1, int[] arr2,int count ) {
        int[] mergedArray = new int[arr1.length + arr2.length];
        int i = 0;
        int j = count;
        int k = 0;
        while (i< arr1.length && j>0 ) {
            if (arr1[i]<arr2[j])
                mergedArray[k++] = arr1[i];
            else
                mergedArray[k++] = arr2[j];

        while(i< arr1.length){
            mergedArray[k++] = arr1[i++];
        }
        while (j>0) {
            mergedArray[k++] = arr2[j--];
        }
        }
        System.out.println(Arrays.toString(mergedArray));
    }
}
