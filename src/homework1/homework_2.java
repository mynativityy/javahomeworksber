package homework1;


import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
public class homework_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        while (n < 8) {
            System.out.println("Пароль с " + n + " количеством символов небезопасен");
            n = scanner.nextInt();
        }
        System.out.println(generatePassword(n));

    }

    public static String generatePassword(int n) {
        String str = "";
        String str1 = "";
        str1 = String.valueOf((char) ThreadLocalRandom.current().nextInt( 'a', 'z'));
        str += str1;
        str1 = String.valueOf((char) ThreadLocalRandom.current().nextInt( 'A', 'Z'));
        str += str1;
        str1 = String.valueOf((char) ThreadLocalRandom.current().nextInt( '0', '9'));
        str += str1;
        str1 = String.valueOf((char) ThreadLocalRandom.current().nextInt( '!', '+'));
        str += str1;
        for (int i = 0; i<n-4;i++) {
            str1 = String.valueOf((char) ThreadLocalRandom.current().nextInt( '!', 'z'));
            str += str1;
        }
        return str;
    }
}
