package homework1;

import java.util.ArrayList;
import java.util.Scanner;
import static homework1.homework_2_2.Generate.generatePassword;

public class homework_2_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        while (n < 8) {
            System.out.println("Пароль с " + n + " количеством символов небезопасен");
            n = scanner.nextInt();}
        System.out.println(generatePassword(n));

    }
    class Generate{
        public static String getRandomInt (){
            String result = String.valueOf((int)(Math.random()* 10));
            return result;
        }
        public static String getRandomChar(){
            String[] character = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
            String result = character[(int)(Math.random()*character.length)];
            return result;
        }
        public static String getRandomCharUpper(){
            String[] character = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            String result = character[(int)(Math.random()*character.length)];
            return result;
        }
        public static String getRandomSym(){
            String[] symbol = {"!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "-", "=", "+"};
            String result = symbol[(int)(Math.random()*symbol.length)];
            return result;
        }
        public static String generatePassword(int passLen){
            ArrayList<String> result = new ArrayList<>();
            String temp = "";
            String x1="";
            x1 = getRandomInt()+getRandomChar()+getRandomCharUpper()+getRandomSym();
            result.add(x1);
            for (int i = 0; i < passLen-4; i++){
                int switcher = (int)(Math.random()*4);
                if (switcher == 0){
                    temp = getRandomInt();
                }
                else if (switcher == 1){
                    temp = getRandomChar();
                }
                else if (switcher == 2){
                    temp = getRandomSym();
                }
                else {
                    temp = getRandomCharUpper();
                }
                result.add(temp);
            }
            StringBuilder sb = new StringBuilder();
            for (String x: result){
                sb.append(x);
            }
            return sb.toString();
        }
    }
}
