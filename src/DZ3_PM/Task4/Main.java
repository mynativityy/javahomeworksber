package DZ3_PM.Task4;

import com.sun.jdi.InterfaceType;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Class<?>> result = getInterfaces(NewClass.class);
        for (Class<?> cls : result) {
            System.out.println(cls.getName());


        }
    }

    public static List<Class<?>> getInterfaces(Class<?> clazz) {
        List<Class<?>> interfaces = new ArrayList<>();
        List<Class<?>> interfaces111 = new ArrayList<>();
        List<Class<?>> interfaces222 = new ArrayList<>();
        while (clazz != Object.class) {
            interfaces.addAll(Arrays.asList(clazz.getInterfaces()));
            clazz = clazz.getSuperclass();
        }
            for (Class<?> interfaceCls : interfaces) {
                Class<?> interface1 = interfaceCls;
                while (interface1 != null) {
                    interfaces111.addAll(Arrays.asList(interface1.getInterfaces()));
                    interface1 = interface1.getSuperclass();
                }
            }
        for (Class<?> interfaceCls2 : interfaces111) {
            Class<?> interface2 = interfaceCls2;
            while (interface2 != null) {
                interfaces222.addAll(Arrays.asList(interface2.getInterfaces()));
                interface2 = interface2.getSuperclass();
                }
            }

        interfaces.addAll(interfaces111);
        interfaces.addAll(interfaces222);
        return interfaces;

    }

}




/*
                System.out.println(getAllInterfaces(NewClass.class));
            }

            public static Set<Class<?>> getAllInterfaces(Class<?> cls) {
                Set<Class<?>> interfaces = new HashSet<>();
                while (cls != Object.class) {
                    Set<Class<?>> tmp = new HashSet<>(List.of(cls.getInterfaces()));
                    for (Class<?> interfaceCls : tmp) {
                        Class<?> checkedInterface = interfaceCls;
                        while (checkedInterface != null) {
                            tmp.addAll(List.of(checkedInterface.getInterfaces()));
                            checkedInterface = checkedInterface.getSuperclass();
                        }
                    }
                    interfaces.addAll(tmp);
                    cls = cls.getSuperclass();
                }
                return interfaces;
            }
        }

*/