package DZ3_PM.Task2;


import DZ3_PM.Task1.IsLike;

@IsLike()
public class Main {

    public static void main(String[] args) {
        Annotations(DZ3_PM.Task1.Main.class);
        Annotations(Main.class);
    }

    public static void Annotations(Class<?> cls) {
        if (!cls.isAnnotationPresent(IsLike.class)) {
            return;
        }
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println("Значение аннотации: " + isLike.something());
    }
}
