package DZ3_PM.Task3;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Class<APrinter> ap = APrinter.class;
        try {
            Method method = ap.getMethod("print", int.class);
            method.setAccessible(true);
            Scanner scanner = new Scanner(System.in);
            method.invoke(ap.newInstance(),scanner.nextInt());
        }
        catch (Exception e){
            System.out.println("Ошибка");
        }

    }
}


