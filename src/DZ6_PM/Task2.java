package DZ6_PM;

import java.math.BigInteger;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n == 1){
            System.out.println("1 не может проверятся на простоту");
        System.exit(1);}
        //1 способ
        Integer integer = n;
        BigInteger bigInteger = BigInteger.valueOf(integer);
        boolean probablePrime = bigInteger.isProbablePrime((int) Math.log(integer));
        System.out.println(probablePrime);
        simple(n);
        System.out.println(isPrime(n));
    }
//2 способ
    public static void simple(int n) {
        int count = 1;
        for (int i = 1; i < n / 2; i++) {
            if (n % i == 0) {
                count++;
            }
        }
        if (count > 2)
            System.out.println("непростое");
        else
            System.out.println("простое");
    }
    //3 способ

    public static boolean isPrime(final int number) {
        return IntStream.rangeClosed(2, number / 2).anyMatch(i -> number % i == 0);
    }
}
